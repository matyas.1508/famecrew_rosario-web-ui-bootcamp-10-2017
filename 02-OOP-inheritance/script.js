class EventEmitter {
    constructor() {
        this.events = {};
    }
    on(eventName, callback) {
        if (this.events[eventName]) {
            this.events[eventName].push(callback);
        } else {
            this.events[eventName] = [callback];
        }
    };
    emit(eventName) {
        if (this.events[eventName]) {
            this.events[eventName].forEach(callback => {
                callback(eventName);
            });
        } else {
            console.log("The event doesn't exist");
        }
    };
    off(eventName, callback) {
        let callbacks = this.events[eventName];
        let index = callbacks.indexOf(callback);
        callbacks.splice(index, 1);
        console.log(index);
    };
}

class Logger {
    constructor() {
    }
    log(info) {
        let logInfo = `The ${info} event has been emitted`;
        console.log(logInfo);
    }
}

class Movie extends EventEmitter {
    constructor(title, year, duration) {
        super();
        this.title = title;
        this.year = year;
        this.duration = duration;
        this.cast = [];
    }
    addCast(cast) {
        if (cast instanceof Actor) {
            this.cast.push(cast);
            console.log(`${cast.name} is now part of the cast`);
        } else if (Array.isArray(cast)) {
            cast.forEach(actor => {
                if (actor instanceof Actor) {
                    this.cast.push(actor);
                    console.log(`${actor.name} is now part of the cast`);
                } else {
                    console.log(`${actor} is not an Actor`);
                }
            });
        } else {
            console.log("The argument is not an Actor or an Array.");
        }
    };
    play() {
        this.emit("play");
    };
    pause() {
        this.emit("pause");
    };
    resume() {
        this.emit("resume");
    };
}

class Actor {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }
}

function start() {
    console.log(`Movie ${this.title} started.`);
}

function stop() {
    console.log(`Movie ${this.title} paused.`);
}

function resume() {
    console.log(`Movie ${this.title} resumed.`);
}

const intime = new Movie("intime", "2011", 109);
const logger = new Logger;

intime.on("play", logger.log);
intime.on("pause", logger.log);
intime.on("resume", logger.log);

intime.play();
intime.pause();
intime.resume();

console.log(intime);

const justin = new Actor('Justin Timberlake', 36);
const actors = [
    new Actor('Amanda Seyfried', 31),
    new Actor('Cillian Murphy', 41)
];
