import {EventEmitter} from "./EventEmitter.js";
import {Actor} from "./Actor.js";

export class Movie extends EventEmitter {
    constructor(title, year, duration) {
        super();
        this.title = title;
        this.year = year;
        this.duration = duration;
        this.cast = [];
    }
    addCast(cast) {
        if (cast instanceof Actor) {
            this.cast.push(cast);
            console.log(`${cast.name} is now part of the cast`);
        } else if (Array.isArray(cast)) {
            cast.forEach(actor => {
                if (actor instanceof Actor) {
                    this.cast.push(actor);
                    console.log(`${actor.name} is now part of the cast`);
                } else {
                    console.log(`${actor} is not an Actor`);
                }
            });
        } else {
            console.log("The argument is not an Actor or an Array.");
        }
    };
    play() {
        this.emit("play");
    };
    pause() {
        this.emit("pause");
    };
    resume() {
        this.emit("resume");
    };
}