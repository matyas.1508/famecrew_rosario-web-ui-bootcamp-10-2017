export class Logger {
    constructor() {
    }
    log(info) {
        let logInfo = `The ${info} event has been emitted`;
        console.log(logInfo);
    }
}