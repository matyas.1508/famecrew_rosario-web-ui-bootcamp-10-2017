import {EventEmitter} from "EventEmitter.js";
import {Actor} from "Actor.js";
import {Movie} from "Movie.js";
import {Logger} from "Logger.js";

const intime = new Movie("intime", "2011", 109);
const logger = new Logger;

intime.on("play", logger.log);
intime.on("pause", logger.log);
intime.on("resume", logger.log);

intime.play();
intime.pause();
intime.resume();

console.log(intime);

const justin = new Actor('Justin Timberlake', 36);
const actors = [
    new Actor('Amanda Seyfried', 31),
    new Actor('Cillian Murphy', 41)
];
