// Local storage
const localStorageText = document.getElementById('localStorageText');
document.onload = recoverText();

function localStorageSave() {
    localStorage.setItem('textArea', localStorageText.value);
    console.log(`Saved \'${localStorageText.value}\' in local storage, remember to clear before go`);
}

function localStorageClear() {
    localStorage.removeItem('textArea');
    localStorageText.value = '';
    console.log(`Local storage cleared`);
}

function recoverText() {
    localStorageText.value = localStorage.getItem('textArea');
}


// Indexed DB
let indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
let db;
let fameDB = indexedDB.open('FameDataBase', 1);
const indexedDBText = document.getElementById('indexedDBText');

fameDB.onupgradeneeded = function (event) {
    db = event.target.result;
    if (!db.objectStoreNames.contains('texts')) {
        db.createObjectStore('texts', { keyPath: 'id', autoIncrement: true });
    }
}

fameDB.onerror = function (event) {
    console.log(event);
}

fameDB.onsuccess = function (event) {
    db = event.target.result;
    console.log('IndexedDB connected');
    recoverIndexedDB();
}

function indexedDBSave(clear) {
    let transaction = db.transaction(['texts'], 'readwrite');
    let store = transaction.objectStore('texts');
    let textContent = {
        id: 1,
        text: ''
    };
    console.log(`Indexed DB cleared`);
    if (!clear) {
        textContent = {
            id: 1,
            text: indexedDBText.value
        };
        indexedDBText.value = textContent.text;
        console.log(`Saved \'${indexedDBText.value}\' in indexed DB, remember to clear before go`);
    } else {
        indexedDBText.value = '';
    }
    let fameDB = store.put(textContent);
    fameDB.onerror = function (event) {
        console.log(event.target.error.name);
    }
    fameDB.onsuccess = function (event) {
        console.log(event);
    }
}

function recoverIndexedDB() {
    let transaction = db.transaction(['texts'], 'readonly');
    let store = transaction.objectStore('texts');
    let fameDB = store.get(1);
    fameDB.onerror = function (event) {
        console.log('error');
    }
    fameDB.onsuccess = function (event) {
        indexedDBText.value = fameDB.result.text;
    }
}


// Drag n' Drop
document.onload = checkFileApi();

function checkFileApi() {
    if (File && FileReader && FileList && Blob) {
        console.log('File API compatible!')
    } else {
        alert('The File APIs are not fully supported in this browser. Drag and drop may not work');
    }
}

let dropper = document.getElementById('droppedFiles');
let dropperOutput = document.getElementById('droppedFilesOutput');
dropper.addEventListener('dragover', handleDragOver, false);
dropper.addEventListener('drop', handleFileSelect, false);

function handleFileSelect(event) {
    event.stopPropagation();
    event.preventDefault();
    let files = event.dataTransfer.files;
    let output = [];
    console.log(files);
    for (let i = 0, f; f = files[i]; i++) {
        if (f.type != 'text/plain') {
            console.log('Only text files allowed')
        } else {
            console.log(`Approved ${f.name}!`)
            output.push(f.name);
        }
    }
    let outputList = '<ul class=\'output-list\'>';
    for (let j = 0; j < output.length; j++) {
        outputList = `${outputList}<li>${output[j]}</li>`;
    }
    outputList = `${outputList}</ul>`;
    dropperOutput.innerHTML = outputList;
}

function handleDragOver(event) {
    event.stopPropagation();
    event.preventDefault();
}


// WebSocket
let socketOutput = document.getElementById('socketOutputContainer');
let ws;
window.onbeforeunload = function (event) {
    ws.close();
};

socketConnect = function () {
    if (!ws || ws.readyState == 3) {
        ws = new WebSocket('ws://echo.websocket.org');
        ws.onopen = function () {
            let node = document.createElement('p');
            let text = document.createTextNode('Web socket connected');
            node.appendChild(text);
            socketOutput.appendChild(node);
        }

        ws.onerror = function (error) {
            let node = document.createElement('p');
            let text = document.createTextNode('There was an error');
            node.style.color = 'red';
            node.appendChild(text);
            socketOutput.appendChild(node);
        }

        ws.onmessage = function (e) {
            let node = document.createElement('p');
            let text = document.createTextNode(`Server replied: ${e.data}`);
            node.appendChild(text);
            socketOutput.appendChild(node);
        }

        ws.onclose = function () {
            let node = document.createElement('p');
            let text = document.createTextNode('Web socket disconnected');
            node.appendChild(text);
            socketOutput.appendChild(node);
            ws = null;
        }

    } else {
        let node = document.createElement('p');
        let text = document.createTextNode('You\'re already connected!');
        node.appendChild(text);
        node.style.color = 'red';
        socketOutput.appendChild(node);
    }
}

function socketSend() {
    if (ws) {
        let message = document.getElementById('socketInput').value;
        let node = document.createElement('p');
        let text = document.createTextNode(`You sent: ${message}`);
        node.appendChild(text);
        socketOutput.appendChild(node);
        ws.send(message);
    } else {
        let node = document.createElement('p');
        let text = document.createTextNode('You cant send the message, you\'re not connected');
        node.appendChild(text);
        node.style.color = 'red';
        socketOutput.appendChild(node);
    }
}

function socketClose() {
    if (ws) {
        ws.close();
    } else {
        let node = document.createElement('p');
        let text = document.createTextNode('You\'re already diconnected!');
        node.appendChild(text);
        node.style.color = 'red';
        socketOutput.appendChild(node);
    }
}


// Canvas
function drawCanvas() {
    const canvas = document.getElementById('canvas');
    if (canvas.getContext) {
        const ctx = canvas.getContext('2d');
        //Rectangles
        ctx.fillStyle = randomColor();
        ctx.fillRect(0, 100, 80, 50);
        ctx.fillStyle = randomColor();
        ctx.fillRect(90, 50, 80, 50);
        ctx.fillStyle = randomColor();
        ctx.fillRect(180, 0, 80, 50);
        //Circles
        for (let i = 0; i < Math.random() * 100; i++) {
            ctx.fillStyle = randomColor();
            ctx.beginPath();
            ctx.arc((Math.random() * 300), (Math.random() * 300), 10, 0, (Math.PI * 2), false);
            ctx.fill();
        }
    }
}

function randomColor() {
    let r = Math.floor(Math.random() * 256);
    let g = Math.floor(Math.random() * 256);
    let b = Math.floor(Math.random() * 256);
    let a = Math.random().toFixed(1);
    let randomColor = `rgba(${r}, ${g}, ${b}, ${a})`;
    return randomColor;
}

drawCanvas();