export interface Movie {
    'id': number;
    'title': string;
    'genre': string;
    'year': number;
    'duration': number;
    'cast': Object[];
}
