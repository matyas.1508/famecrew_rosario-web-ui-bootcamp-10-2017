import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MoviesService } from '../../services/movies.service';
import { Movie } from '../../movie.interface';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']
})
export class MovieComponent implements OnInit {

  movie?: Movie;
  editDetail = false;

  constructor(private route: ActivatedRoute, private moviesService: MoviesService) { }

  private getMovie(id: number): void {
    this.moviesService.getMovie(id).subscribe(movie => {
      this.movie = movie;
    });
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      const id = params['id'];
      this.getMovie(id);
    });
  }

  toggleEdit(): void {
    this.editDetail = !this.editDetail;
  }
}
