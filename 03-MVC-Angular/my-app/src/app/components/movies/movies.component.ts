import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../../services/movies.service';
import { Movie } from '../../movie.interface';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss']
})
export class MoviesComponent implements OnInit {

  movies: Movie[] = [];

  constructor(private moviesService: MoviesService) { }

  ngOnInit() {
    this.moviesService.getMovies().subscribe(movies => {
      this.movies = movies;
    });
  }

  deleteMovie(i: number): void {
    this.movies.splice(i, 1);
  }

  addMovie(movie: string): void {
    const newMovie = {
      id: this.movies.length + 1,
      title: movie,
      genre: 'unknow',
      year: 0,
      duration: 0,
      cast: []
    };
    this.movies.push(newMovie);
  }

}
