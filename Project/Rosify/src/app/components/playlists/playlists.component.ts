import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';
import { Playlist } from '../../playlist';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-playlists',
  templateUrl: './playlists.component.html',
  styleUrls: ['./playlists.component.scss']
})
export class PlaylistsComponent implements OnInit {

  accessToken: string;
  playlistRes: Playlist[] = [];
  playlistForm: FormGroup;
  userInfo;

  constructor(private formBuilder: FormBuilder, private spotifyService: SpotifyService) {
    this.createForm();
  }

  private createForm() {
    this.playlistForm = this.formBuilder.group({
      'name': [null, [Validators.required]],
      'public': [false, [Validators.required]],
      'description': [null],
    });
  }

  ngOnInit() {
    this.spotifyService.getToken();
    this.spotifyService.getUserPlaylists()
      .subscribe(res => {
        this.playlistRes = res.items;
      });
    this.spotifyService.getUserInfo()
      .subscribe(res => {
        this.userInfo = res;
      });
  }

  onSubmit() {
    this.spotifyService.createNewPlaylist(this.playlistForm.value, this.userInfo.id).subscribe();
  }

}
