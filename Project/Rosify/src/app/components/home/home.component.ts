import { Component, OnInit } from '@angular/core';
import { TokenService } from '../../services/token.service';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  userInfo: object = undefined;

  constructor(private tokenService: TokenService, private spotifyService: SpotifyService) { }

  ngOnInit() {
    this.spotifyService.getToken();
    if (this.userInfo === undefined) {
      this.spotifyService.getUserInfo()
        .subscribe(res => {
          this.userInfo = res;
        });
    }
  }

  login() {
    this.tokenService.login();
  }
}
