import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';
import { Artist } from '../../artist';
import { Track } from '../../track';
import { Playlist } from '../../playlist';
import { Album } from '../../album';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  searchStr = '';
  searchType = undefined;
  artistRes: Artist[];
  trackRes: Track[];
  playlistRes: Playlist[];
  albumRes: Album[];
  res = {
    artistRes: undefined,
    trackRes: undefined,
    playlistRes: undefined,
    albumRes: undefined
  };

  constructor(private spotifyService: SpotifyService) { }

  ngOnInit() {
    this.spotifyService.getToken();
  }

  searchMusic() {
    if (this.searchStr) {
      if (this.searchType) {
        this.spotifyService.searchMusic(this.searchStr, this.searchType)
          .subscribe(data => {
            this.dataManager(this.searchType, data);
          });
      }
    }
  }

  dataManager(searchType, data) {
    if (this.searchType === 'artist') {
      this.artistRes = data.artists.items;
      this.albumRes = this.res.albumRes;
      this.trackRes = this.res.trackRes;
      this.playlistRes = this.res.playlistRes;
    } else if (this.searchType === 'album') {
      this.albumRes = data.albums.items;
      this.artistRes = this.res.artistRes;
      this.trackRes = this.res.trackRes;
      this.playlistRes = this.res.playlistRes;
    } else if (this.searchType === 'track') {
      this.trackRes = data.tracks.items;
      this.albumRes = this.res.albumRes;
      this.artistRes = this.res.artistRes;
      this.playlistRes = this.res.playlistRes;
    } else if (this.searchType === 'playlist') {
      this.playlistRes = data.playlists.items;
      this.albumRes = this.res.albumRes;
      this.trackRes = this.res.trackRes;
      this.artistRes = this.res.artistRes;
    }
  }
}
