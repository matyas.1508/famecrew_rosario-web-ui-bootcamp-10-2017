import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { SpotifyService } from '../../services/spotify.service';
import { Playlist } from '../../playlist';
import { Track } from '../../track';

@Component({
  selector: 'app-tracks',
  templateUrl: './tracks.component.html',
  styleUrls: ['./tracks.component.scss']
})
export class TracksComponent implements OnInit {

  searchStr = '';
  searchType = 'track';
  playlistRes: Playlist[] = [];
  trackRes: Track[];
  userInfo;
  tracksToAdd = [];
  uriStr = '';
  playlistId;

  constructor(private formBuilder: FormBuilder, private spotifyService: SpotifyService) { }

  ngOnInit() {
    this.spotifyService.getToken();
    this.spotifyService.getUserPlaylists()
      .subscribe(res => {
        this.playlistRes = res.items;
      });
    this.spotifyService.getUserInfo()
      .subscribe(res => {
        this.userInfo = res;
      });
  }

  searchMusic() {
    if (this.searchStr) {
      this.spotifyService.searchMusic(this.searchStr, this.searchType)
        .subscribe(res => {
          this.trackRes = res.tracks.items;
        });
    } else {
      this.trackRes = undefined;
    }
  }

  addTrack(track) {
    this.tracksToAdd.push(track);
  }

  deleteTrack(i) {
    this.tracksToAdd.splice(i, 1);
  }

  uploadTracks() {
    const uris = this.uriConstructor();
    this.spotifyService.uploadTracks(this.userInfo.id, this.playlistId, uris).subscribe();
  }

  uriConstructor() {
    const uris = [];
    this.tracksToAdd.forEach(element => {
      uris.push(element.uri);
    });
    return uris;
  }
}
