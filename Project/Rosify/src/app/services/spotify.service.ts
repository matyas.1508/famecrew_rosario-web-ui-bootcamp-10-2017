import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { TokenService } from './token.service';

@Injectable()
export class SpotifyService {

  private tokenUrl: string;
  private searchUrl: string;
  private accessToken;
  private playlistUrl;

  constructor(private http: Http, private tokenService: TokenService) { }

  getToken() {
    this.accessToken = this.tokenService.returnToken();
  }

  getUserInfo() {
    const headers = new Headers();
    headers.append(`Authorization`, `Bearer ${this.accessToken}`);
    return this.http
      .get('https://api.spotify.com/v1/me', { headers })
      .map((data: Response) => data.json());
  }

  getUserPlaylists() {
    const headers = new Headers();
    headers.append(`Authorization`, `Bearer ${this.accessToken}`);
    return this.http
      .get('https://api.spotify.com/v1/me/playlists', { headers })
      .map((data: Response) => data.json());
  }

  createNewPlaylist(form, id) {
    const headers = new Headers();
    headers.append(`Authorization`, `Bearer ${this.accessToken}`);
    return this.http
      .post(`https://api.spotify.com/v1/users/${id}/playlists`, form, { headers })
      .map((data: Response) => data.json());
  }

  uploadTracks(userId, playlistId, uris) {
    const headers = new Headers();
    headers.append(`Authorization`, `Bearer ${this.accessToken}`);
    return this.http
      .post(`https://api.spotify.com/v1/users/${userId}/playlists/${playlistId}/tracks`, uris, { headers })
      .map((data: Response) => data.json());
  }

  searchMusic(str: string, type: string) {
    this.searchUrl = `https://api.spotify.com/v1/search?q=${str}&offset=0&limit=20&type=${type}`;
    const headers = new Headers();
    headers.append(`Authorization`, `Bearer ${this.accessToken}`);
    return this.http
      .get(this.searchUrl, { headers })
      .map((data: Response) => data.json());
  }

  getAlbum(id: string) {
    const headers = new Headers();
    headers.append(`Authorization`, `Bearer ${this.accessToken}`);
    return this.http
      .get(`https://api.spotify.com/v1/albums/${id}`, { headers })
      .map((data: Response) => data.json());
  }

  getArtist(id: string) {
    const headers = new Headers();
    headers.append(`Authorization`, `Bearer ${this.accessToken}`);
    return this.http
      .get(`https://api.spotify.com/v1/artists/${id}`, { headers })
      .map((data: Response) => data.json());
  }

  getArtistAlbums(id: string) {
    const headers = new Headers();
    headers.append(`Authorization`, `Bearer ${this.accessToken}`);
    return this.http
      .get(`https://api.spotify.com/v1/artists/${id}/albums`, { headers })
      .map((data: Response) => data.json());
  }
}
