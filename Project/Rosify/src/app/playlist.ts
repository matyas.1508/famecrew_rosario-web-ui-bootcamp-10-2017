export class Playlist {
    collaborative: boolean;
    external_urls: {
        spotify: string;
    };
    href: string;
    id: string;
    images: object[];
    name: string;
    owner: {
        display_name: string;
        external_urls: {
            spotify: string;
        };
        href: string;
        id: string;
        type: string;
        uri: string;
    };
    public: any;
    snapshot_id: string;
    tracks: {
        href: string;
        total: number;
    };
    type: string;
    uri: string;
}
