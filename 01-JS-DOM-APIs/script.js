//Hello world/dude fade-in!!
function myFunction() {
    document.getElementById("fadeInHeader").style.opacity = "1";
}

//Chuck's jokes HTTP Request
function getConfig() {
    config = {
        method: 'GET',
        URL: 'http://api.icndb.com/jokes/random',
        Boolean: 'true'
    }
    return onClick(config);
}

function onClick(config) {
    const xhttp = new XMLHttpRequest();
    xhttp.open(config.method,config.URL,config.Boolean);
    xhttp.onload = function (e) {
        if (this.readyState == 4 && this.status == 200) {
            const response = JSON.parse(xhttp.response);
            const resp = response.value;
            console.log(resp.id);
            console.log(resp.joke);
            document.getElementById("fadeInHeader").innerText = resp.joke;
        } else {
            console.error(xhttp.statusText);
            document.getElementById("fadeInHeader").innerText = xhttp.statusText;
            document.getElementById("fadeInHeader").style.backgroundColor = "red";
        }
    }
    xhttp.onerror = function (e) {
        console.error(xhttp.statusText);
        document.getElementById("fadeInHeader").innerText = xhttp.statusText;
        document.getElementById("fadeInHeader").style.backgroundColor = "red";
    };
    xhttp.send();
};

//Github search request
function createCORS(method, url) {
    const xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
        xhr.open(method, url, true);
    }
    else if (typeof XDomainRequest != "undefined") {
        xhr = new XDomainRequest();
        xhr.open(method, url);
    } else {
        xhr = null;
    }
    return xhr;
}

function getRequest(url) {
    return new Promise((resolve, reject) => {
        const xhr = createCORS('GET', url)
        if (!xhr) {
            reject("cors not supported");
        }
        xhr.onload = () => {
            resolve(JSON.parse(xhr.responseText));
        }
        xhr.onerror = () => {
            reject("There was an error!");
        }
        xhr.send();
    })
}

function searchRepos(value) {
    const list = document.getElementById('list');
    list.innerText = "";
    const url = "https://api.github.com/search/repositories?q=".concat(value);
    getRequest(url).then(response => {
        for (let i = 0; i < response.items.length; i++) {
            const node = document.createElement("li");
            const link = document.createElement("a");
            link.href = response.items[i].html_url;
            const textnode = document.createTextNode(response.items[i].name);
            link.appendChild(textnode);
            node.appendChild(link);
            list.appendChild(node);
        }
    }).catch(error => console.log(error))
}

//Create table request
function createTable() {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "https://jsonplaceholder.typicode.com/users", true);
    xhr.onload = function (e) {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                objectResponse = JSON.parse(xhr.responseText);
                const table = document.createElement("table");
                table.setAttribute("class", "table");
                document.getElementById("tableContainer").appendChild(table);
                const row = document.createElement("tr");
                table.appendChild(row);
                const th = document.createElement("th");
                const thContent = document.createTextNode("Name");
                const th2 = document.createElement("th");
                const th2Content = document.createTextNode("Username");
                th.appendChild(thContent);
                th2.appendChild(th2Content);
                row.appendChild(th);
                row.appendChild(th2);
                for (var i = 0; i < objectResponse.length; i++) {
                    var user = objectResponse[i];
                    tableGenerate(user);
                    function tableGenerate() {
                        const row = document.createElement("tr");
                        const name = document.createElement("td");
                        const nameContent = document.createTextNode(user.name);
                        name.appendChild(nameContent);
                        row.appendChild(name);
                        const username = document.createElement("td");
                        const usernameContent = document.createTextNode(user.username);
                        username.appendChild(usernameContent);
                        row.appendChild(username);
                        table.appendChild(row);
                    }
                }
            } else {
                console.error(xhr.statusText);
            }
        }
    };
    xhr.onerror = function (e) {
        console.error(xhr.statusText);
    };
    xhr.send(null);
}